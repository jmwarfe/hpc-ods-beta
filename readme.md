HPC-ODS - Open Data Science framework for the HPC.

The contents of this repository contains the build definition 
and scripts used to create a Singularity container for HPC-ODS.